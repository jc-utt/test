FROM docker-io-nexus.uttnetgroup.net/python:3.6-alpine

EXPOSE 8080

CMD ["python3", "-m", "http.server", "8080"]
